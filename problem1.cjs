function getCarInformation(carInventory, carId) {
    if (carInventory === undefined || carInventory.length === 0 || carId === undefined) {
        return [];
    }
    let carInfo = {};
    for (let idx = 0; idx < carInventory.length; idx++) {
        if (carInventory[idx]?.id === carId) {
            carInfo = carInventory[idx];
        }
    }
    return JSON.stringify(carInfo) !== '{}' ? carInfo : [];
}

module.exports = getCarInformation;